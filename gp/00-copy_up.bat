SET SSS=E:\Gitee\sss1\
set V1=%1

set SOURCE1=%SSS%\fbqd\table\latest
set SOURCE2=%SSS%\kzz\image\latest
set SOURCE3=%SSS%\kzzpz\image\latest
set SOURCE4=%SSS%\index\image\latest
set SOURCE5=%SSS%\stock\image\latest

if "%V1%"=="fbqd" (
	copy %SOURCE1% %~dp0 /Y
	00-upload_fbqd.sh
)else (
	if "%V1%"=="kzz" (
		copy %SOURCE2% %~dp0 /Y
		copy %SOURCE3% %~dp0 /Y
		00-upload_kzz.sh
	) else (
		if "%V1%"=="index" (
			copy %SOURCE4% %~dp0 /Y
			copy %SOURCE5% %~dp0 /Y
			00-upload_index.sh
		)
	)
)

del .gitignore

pause